export const DataService = [
  {
    icon: 'flaticon flaticon-008-file',
    serviceTitle: ' Web Development',
    serviceContent: '',
    serviceItems: [{ name: 'Web Development' }],
  },
  {
    icon: 'flaticon flaticon-055-smartphone-6',
    serviceTitle: ' Mobile Development',
    serviceContent: '',
    serviceItems: [{ name: 'Mobile Development React Native' }],
  },
  {
    icon: 'flaticon flaticon-032-pen',
    serviceTitle: ' UI/UX Design',
    serviceContent: '',
    serviceItems: [{ name: 'UX Strategy and Consulting' }],
  },
  {
    icon: 'flaticon flaticon-063-browser-8',
    serviceTitle: 'Testing & QA',
    serviceContent: '',
    serviceItems: [{ name: 'Testing & QA' }],
  },
  {
    icon: 'flaticon flaticon-009-coding-1',
    serviceTitle: 'IT Consulting',
    serviceContent: '',
    serviceItems: [{ name: 'IT Consulting' }],
  },
  {
    icon: 'flaticon flaticon-054-laptop-7',
    serviceTitle: 'Data Analytics',
    serviceContent: '',
    serviceItems: [{ name: 'Data Analytics' }],
  },
]
