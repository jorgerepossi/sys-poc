import React from "react";
import LogoImg from "./../../assets/images/logo.png";
import styled from "styled-components";
import { Link } from "react-router-dom";

export const StyledLogo = styled.div`
	img {
		max-width: 200px;
	}
	@media (min-width: 768px) {
		img {
			max-width: 250px;
		}
	}
`;
export const Logo = () => {
	return (
		<StyledLogo>
			<Link to="/">
				<img src={LogoImg} alt="Dispacth - Simple Software Service" />
			</Link>
		</StyledLogo>
	);
};
export default Logo;
